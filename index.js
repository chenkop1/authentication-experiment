const jwt = require('jsonwebtoken');
const NodeRSA = require('node-rsa');

const key = new NodeRSA({ b: 512 }); // generate KeyPair
const publicKey = key.exportKey('public');
const privateKey = key.exportKey('private');

const token = jwt.sign({ id: 'whatever' }, privateKey, { algorithm: 'RS256' }); // sign with private key

try {
  const data = jwt.verify(token, publicKey, { algorithms: ['RS256'] }); // verify with public key
  console.log(data);
} catch (error) {
  console.log(error); // token is invalid
}
